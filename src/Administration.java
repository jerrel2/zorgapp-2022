import java.io.OutputStream;
import java.time.LocalDate;
import java.util.Scanner;

///////////////////////////////////////////////////////////////////////////
// class Administration represents the core of the application by showing
// the main menu, from where all other functionality is accessible, either
// directly or via sub-menus.
//
// An Administration instance needs a User as input, which is passed via the
// constructor to the data member 'çurrentUser'.
// The patient data is available via the data member çurrentPatient.
/////////////////////////////////////////////////////////////////
public class Administration
{
   private static final int STOP = 0;
   private static final int VIEW = 1;
   private static final int SURNAME = 2;
   private static final int FIRSTNAME = 3;
   private static final int DATEOFBIRTH = 4;
   private static final int LENGTH = 5;
   private static final int WEIGHT = 6;



   private Patient currentPatient;            // The currently selected patient
   private User    currentUser;               // the current user of the program.

   /////////////////////////////////////////////////////////////////
   // Constructor
   /////////////////////////////////////////////////////////////////
   Administration( User user )
   {
      currentUser    = user;
      currentPatient = new Patient( 1, "Van Puffelen", "Pierre", LocalDate.of( 2000, 2, 29 ), 1.74, 70 );
      System.out.format( "Current user: [%d] %s\n", user.getUserID(), user.getUserName() );
   }

   /////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////
   void menu()
   {
      var scanner = new Scanner( System.in );  // User input via this scanner.

      boolean nextCycle = true;
      while (nextCycle)
      {
         System.out.format( "%s\n", "=".repeat( 80 ) );
         System.out.format( "Current patient: %s\n", currentPatient.fullName() );

         ////////////////////////a
         // Print menu on screen
         ////////////////////////
         System.out.format( "%d:  STOP\n", STOP );
         System.out.format( "%d:  View patient data\n", VIEW );
         System.out.format( "%d:  Edit patient surname\n", SURNAME );
         System.out.format( "%d:  Edit patient first name\n", FIRSTNAME );
         System.out.format( "%d:  Edit patient Date of birth\n", DATEOFBIRTH );
         System.out.format( "%d:  Edit patient Length\n", LENGTH);
         System.out.format( "%d:  Edit patient Weight\n", WEIGHT );

         ////////////////////////

         System.out.print( "enter #choice: " );
         int choice = scanner.nextInt();
         switch (choice)
         {
            case STOP: // interrupt the loop
               nextCycle = false;
               break;

            case VIEW:
               currentUser.viewPatientData( currentPatient );
               break;

            case SURNAME:
               System.out.println("Enter new Surname:");

               Scanner scanner2 = new Scanner(System.in);

               String surname = scanner2.next();

               currentPatient.



            default:
               System.out.println( "Please enter a *valid* digit" );
               break;
         }
      }

   }

}
