public class ZorgApp
{
   public static void main( String[] args )
   {
      User           user           = new User( 1, "Mart ElCamera" );
      User           user1          = new User( 2, "Jacob Boerma" );
      User           user2          = new User( 3, "Pieter Post");
      User           user3          = new User( 4, "Karel Barends");
      User           user4          = new User( 5, "Mike Bezem");

      Administration administration = new Administration( user );

      administration.menu();
   }
}
