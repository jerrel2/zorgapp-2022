import org.w3c.dom.ls.LSOutput;

import java.sql.SQLOutput;
import java.time.LocalDate;
import java.time.Period;

public class Patient
{
   private static final int RETURN      = 0;
   private static final int SURNAME     = 1;
   private static final int FIRSTNAME   = 2;
   private static final int DATEOFBIRTH = 3;
   private static final int AGE         = 4;
   private static final int BMI         = 5;
   private static final int LENGTH      = 6;
   private static final int WEIGHT      = 7;

   private int       id;
   private String    surname;
   private String    firstName;
   private LocalDate dateOfBirth;
   private double    Length;
   private double    Weight;

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public String getSurname()
   {
      return surname;
   }
   public String setSurname() {

   }
   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public String getFirstName()
   {
      return firstName;
   }

   ////////////////////////////////////////////////////////////////////////////////
   // Constructor
   ////////////////////////////////////////////////////////////////////////////////
   Patient( int id, String surname, String firstName, LocalDate dateOfBirth, double Length, double Weight )
   {
      this.id          = id;
      this.surname     = surname;
      this.firstName   = firstName;
      this.dateOfBirth = dateOfBirth;
      this.Length      = Length;
      this.Weight      = Weight;

   }

   ////////////////////////////////////////////////////////////////////////////////
   // Display patient data.
   ////////////////////////////////////////////////////////////////////////////////
   public void viewData()
   {
      System.out.format( "===== Patient id=%d ==============================\n", id );
      System.out.format( "%-17s %s\n", "Surname:", surname );
      System.out.format( "%-17s %s\n", "firstName:", firstName );
      System.out.format( "%-17s %s\n", "Date of birth:", dateOfBirth );
      System.out.format( "%-17s %d\n", "Age:", CalcAge() );
      System.out.format( "%-17s %2f\n", "BMI", CalcBmi() );
      System.out.format( "%-17s %2f\n", "Length", Length );
      System.out.format( "%-17s %2f\n", "Weight", Weight );
   }

   ////////////////////////////////////////////////////////////////////////////////
   // Shorthand for a Patient's full name
   ////////////////////////////////////////////////////////////////////////////////
   public String fullName()
   {
      return String.format( "%s %s [%s]", firstName, surname, dateOfBirth.toString() );
   }

   ////////////////////////////////
   // Leeftijd berekenen
   //////////////////////////////
   public int CalcAge() {
      int age = Period.between(dateOfBirth, LocalDate.now()).getYears();
      return age;
   }
   public double CalcBmi() {
      double bmi = Weight / ( Length * Length );
      return bmi;
   }
}
